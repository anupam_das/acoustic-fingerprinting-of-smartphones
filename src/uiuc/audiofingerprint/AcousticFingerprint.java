package uiuc.audiofingerprint;


import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder.AudioSource;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.os.Build;


public class  AcousticFingerprint  extends Activity {
 
 Integer[] freqset = {44100, 22050, 11025, 8000};
 String [] audios= {"human1","human2","human3","human4",
		            "ringtone1","ringtone2","ringtone3","ringtone4",
		            "song1","song2","song3"};
 String [] volume= {"Full","Medium", "Low"};
 
 private enum Audiotype{
		human1, human2, human3,human4, ringtone1,ringtone2,ringtone3,ringtone4, song1,song2,song3;
 }

 private ArrayAdapter<Integer> adapter;
 private ArrayAdapter<String> auadapter;
 private ArrayAdapter<String> volumeadapter;

 public NetworkInfo mWifi;

 private static final String TAG = "AudioFingerprint";  
  
 Map<String, Integer> counter;
 
 TelephonyManager tm;
 UUID deviceUuid;
 String deviceId,modelID;
 Spinner spFrequency, audioClips, volumeController;
 Button uploadData, playBack, exit,counterupdate;
 int runningtasks=0;
 Boolean recording,buttonPlay,buttonExit,buttonUpload;
 
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
      
        playBack = (Button)findViewById(R.id.playback);
        uploadData = (Button)findViewById(R.id.upload);
        exit = (Button)findViewById(R.id.exit);
        counterupdate=(Button)findViewById(R.id.displaycount);

        playBack.setOnClickListener(playBackOnClickListener);
		uploadData.setOnClickListener(uploadDataOnClickListener);
		exit.setOnClickListener(exitOnClickListener);
		counterupdate.setOnClickListener(counterOnClickListener);
		
        spFrequency = (Spinner)findViewById(R.id.frequency);
        adapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, freqset);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFrequency.setAdapter(adapter);
        
        audioClips = (Spinner)findViewById(R.id.audioclips);
        auadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, audios);
        auadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        audioClips.setAdapter(auadapter);
        
        volumeController = (Spinner)findViewById(R.id.volumes);
        volumeadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, volume);
        volumeadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        volumeController.setAdapter(volumeadapter);

		buttonPlay=true;//true if enabled
		buttonExit=true;
		buttonUpload=true;
		//uploadData.setEnabled(false);
		//exit.setEnabled(false);
		counter= new HashMap<String, Integer>();
		try{
			tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
			final String tmDevice, tmSerial, androidId;
			tmDevice = "" + tm.getDeviceId();
			tmSerial = "" + tm.getSimSerialNumber();
			androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

			deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
			deviceId = deviceUuid.toString();
			modelID=Build.BRAND+"_"+Build.DEVICE+"_"+Build.MANUFACTURER+"_"+Build.MODEL+"_"+Build.PRODUCT;
			     
		  }catch (Exception e) {
			e.printStackTrace();
		  }  

    }
    
    @Override
	  //What action to take on resuming the app
	protected void onResume() {
		super.onResume();
		ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
		mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	}

    OnClickListener playBackOnClickListener = new OnClickListener(){
		@Override
	   	public void onClick(View v) {
			updateServiceStatus(1);
			Thread myplayThread = new Thread(new Runnable() {
		  		@Override
	      		public void run() {
		  			playWav();
		  		}
	  		});
			myplayThread.start();
		}
 	};

	OnClickListener uploadDataOnClickListener = new OnClickListener(){
		@Override
	   	public void onClick(View v) {
			Thread mysendThread = new Thread(new Runnable() {
		  		@Override
	      		public void run() {
		  			sendData();
		  		}
	  		});
			mysendThread.start();
		}
 	};
 
    OnClickListener exitOnClickListener = new OnClickListener(){
		@Override
	   	public void onClick(View v) {
			Thread myexitThread = new Thread(new Runnable() {
		  		@Override
	      		public void run() {
		  			//if(runningtasks > 0) sendData();
					//while (runningtasks > 0){}
					mWifi=null;
					System.exit(0);
		  		}
	  		});
			myexitThread.start();
		}
 	};

 	OnClickListener counterOnClickListener = new OnClickListener(){
		@Override
	   	public void onClick(View v) {
			Thread mycounterThread = new Thread(new Runnable() {
		  		@Override
	      		public void run() {
		  			final String sampleaudio = (String)audioClips.getSelectedItem();
		  			updateCounter(sampleaudio,0);
		  		}
	  		});
			mycounterThread.start();
		}
 	};
 	
	public void disableButton(final int resourceId){
		Thread buttonThread = new Thread(new Runnable() {
			@Override
			public void run() {
				final Button tmp = (Button)findViewById(resourceId);
				tmp.post( new Runnable() { 
	                @Override
	                public void run() {
	                	tmp.setEnabled(false);
	                }
	            });
			}   
        });
		buttonThread.start();
	}
  
	public void enableButton(final int resourceId){
		Thread buttonThread = new Thread(new Runnable() {
			@Override
			public void run() {
				final Button tmp = (Button)findViewById(resourceId);
				tmp.post( new Runnable() { 
	                @Override
	                public void run() {
	                	tmp.setEnabled(true);
	                }
	            });
			}   
        });
		buttonThread.start();
	}
	
	
	public void audioText(final String filename){
		Thread textThread = new Thread(new Runnable() {
			@Override
			public void run() {
				final TextView audiotype = (TextView)findViewById(R.id.filename);
				audiotype.post(new Runnable(){
					//@Override
	               public void run() {
	            	   audiotype.setText(filename);
					}
				});
			}   
        });
		textThread.start();
	}
	
	public void audioCounter(final int val){
		Thread textThread = new Thread(new Runnable() {
			@Override
			public void run() {
				final TextView count = (TextView)findViewById(R.id.counter);
				count.post(new Runnable(){
					//@Override
					public void run() {
						count.setText(val+"");
					}
				});
			}   
        });
		textThread.start();
	}
	
	//Display current counter
	private void updateCounter(String type,int v){
		if(counter.containsKey(type)){
			Integer tmp=counter.get(type).intValue()+v;
			counter.put(type, tmp);
		}
		else{
			Integer tmp=v;
			counter.put(type, tmp);
		}
		int value=counter.get(type).intValue();
		//Log.i(TAG,"Counter="+value);
		audioText(type);
		audioCounter(value);
	}
		  
	
	//Display current status
	private void updateServiceStatus(final int val){
		Thread textThread = new Thread(new Runnable() {
			@Override
			public void run() {
				final TextView status = (TextView)findViewById(R.id.status);
				status.post(new Runnable(){
					//@Override
	               public void run() {
						if(val==0) status.setText("...");
						else if(val==1) status.setText("Recording...");
						else if (val==2) status.setText("Recorded");
						else if (val==3) status.setText("Uploading...");
						else if (val==4) status.setText("Uploaded");
						else if (val==5) status.setText("Nothing to Upload");
						else if (val==6) status.setText("Wait until upload finishes...");
					}
				});
			}
		});
		textThread.start();
    }
	  
	  
	//Start playing audio samples all WAV format with 16-bit PCM Stereo format with 44.1kHz sampling frequency
 	public void playWav(){
		//disable button until everything finishes
 		if(buttonUpload){disableButton(R.id.upload);buttonUpload=false;}
 		if(buttonExit){disableButton(R.id.exit);buttonExit=false;}
 		
		int sampleFreq = 44100;//(Integer)spFrequency.getSelectedItem();
		int minBufferSize = AudioTrack.getMinBufferSize(sampleFreq, AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_16BIT);
	    int bufferSize = 512;
	    if (minBufferSize<=0){
			Log.i(TAG, "Increasing buffer to hold enough samples " + bufferSize + " was: " + minBufferSize);
			minBufferSize = bufferSize;
		}
	    AudioTrack at = new AudioTrack(AudioManager.STREAM_MUSIC, sampleFreq, AudioFormat.CHANNEL_IN_STEREO, AudioFormat.ENCODING_PCM_16BIT, minBufferSize, AudioTrack.MODE_STREAM);
	
	    final String sampleaudio = (String)audioClips.getSelectedItem();
	    Audiotype audiofile= Audiotype.valueOf(sampleaudio); 
	    int resourceID=R.raw.human1;
	    switch(audiofile){
	    	case human1: resourceID=R.raw.human1; break;
	    	case human2: resourceID=R.raw.human2; break;
	    	case human3: resourceID=R.raw.human3; break;
	    	case human4: resourceID=R.raw.human4; break;
	    	case ringtone1: resourceID=R.raw.ringtone1; break;
	    	case ringtone2: resourceID=R.raw.ringtone2; break;
	    	case ringtone3: resourceID=R.raw.ringtone3; break;
	    	case ringtone4: resourceID=R.raw.ringtone4; break;
	    	case song1: resourceID=R.raw.song1; break;
	    	case song2: resourceID=R.raw.song2; break;
	    	case song3: resourceID=R.raw.song3; break;
	    }
	    InputStream input=getResources().openRawResource(resourceID);
	    			
	    final String samplevolume = (String)volumeController.getSelectedItem();
	    double fractionVolume=1.0;
	    if(samplevolume.equals("Full")) fractionVolume=1.0;
	    else if (samplevolume.equals("Medium")) fractionVolume=0.85;
	    else if (samplevolume.equals("Low")) fractionVolume=0.7;
	    
	    AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	    am.setStreamVolume(AudioManager.STREAM_MUSIC, (int)(am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)*fractionVolume),  0);
	    
	    int i = 0;
	    byte[] s = new byte[minBufferSize];
	    try{
			Thread recordThread = new Thread(new Runnable(){
	  		  @Override
	  		  public void run() {
	  			  recording = true;
	  			  startRecord(sampleaudio,samplevolume);
	  		  } 
	  	    });
	  	    recordThread.start();
	  	    try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  	    input.skip(44);//skip header
		    at.play();
		    while((i = input.read(s, 0, minBufferSize))>-1){
		    	int z=i%4;
		    	if(i<minBufferSize && z!=0){
		    		for(int j=i;j<z;j++)
		    			s[j]=(byte)0;	 
		    	}
	            at.write(s, 0, i+z);
		    }
		    at.stop();
		    at.release();
		    input.close();
		    recording = false;
   			if(!buttonUpload){enableButton(R.id.upload);buttonUpload=true;}
   			if(!buttonExit){enableButton(R.id.exit);buttonExit=true;}
   			updateCounter(sampleaudio,1);
   			
	    } catch (FileNotFoundException e) {
	        // TODO
	        e.printStackTrace();
	    } catch (IOException e) {
	        // TODO
	        e.printStackTrace();
	    }       
	}
 
    // record played audio in 16-bit PCM WAV format using selected frequency and volume
	private void startRecord(String filename,String volume){
		Log.i(TAG,"Recording audio");
		int sampleFreq = (Integer)spFrequency.getSelectedItem();
		long dtMili = System.currentTimeMillis();
		File file = new File(Environment.getExternalStorageDirectory(), filename+"_"+deviceId+"_"+modelID+"_"+dtMili+"_"+volume+"_"+sampleFreq+".pcm"); 

		//Log.i(TAG, "Attempting rate " + sampleFreq + "Hz, bits.");  
		try{
			file.createNewFile();

			OutputStream outputStream = new FileOutputStream(file);
			BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
			DataOutputStream dataOutputStream = new DataOutputStream(bufferedOutputStream);

			int minBufferSize = AudioRecord.getMinBufferSize(sampleFreq, 
				   											AudioFormat.CHANNEL_IN_MONO, 
				   											AudioFormat.ENCODING_PCM_16BIT);
			
			int detectAfterEvery = (int)((float)sampleFreq * 1.0f);

			if (detectAfterEvery > minBufferSize){
				Log.i(TAG, "Increasing buffer to hold enough samples " + detectAfterEvery + " was: " + minBufferSize);
				minBufferSize = detectAfterEvery;
			}
			
			byte[] audioData = new byte[minBufferSize];
			AudioRecord audioRecord = new AudioRecord(AudioSource.MIC,
				   									 sampleFreq,
													 AudioFormat.CHANNEL_IN_MONO,
													 AudioFormat.ENCODING_PCM_16BIT,
													 minBufferSize);
			
			audioRecord.setPositionNotificationPeriod(detectAfterEvery);
			
			audioRecord.startRecording();
			while(recording){
				int numberOfShort = audioRecord.read(audioData, 0, minBufferSize);
				dataOutputStream.write(audioData, 0, numberOfShort);//.writeShort(audioData[i]);
   			}
   			audioRecord.stop();
   			dataOutputStream.close();
   			bufferedOutputStream.close();
   			outputStream.close();
   			updateServiceStatus(2);
  		} catch (Exception e){
   			e.printStackTrace();
  		}

	}

    // Send recored audio to remote server
	public void sendData(){
		try{	
			Log.i(TAG,"Uploading data");
			if (mWifi.isConnected()) {
			
				if(buttonPlay){disableButton(R.id.playback);buttonPlay=false;}
				if(buttonExit){disableButton(R.id.exit);buttonExit=false;}
				
				String myDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
				File f = new File(myDirectory);
				File[] flists=null;
				if(f.exists() && f.isDirectory()){
					final Pattern p = Pattern.compile(".*?.pcm"); 
					flists = f.listFiles(new FileFilter(){
						@Override
						public boolean accept(File file) {
							return p.matcher(file.getName()).matches();
						}
					});
				}
				//Log.i(TAG,"found "+flists.length+" files");
				for(int i=0;i<flists.length;i++){
					synchronized(this){runningtasks++;}
					try{
						final File uploadfile=flists[i];
						AcousticFingerprint.this.runOnUiThread(new Runnable() {
							 public void run(){
								 new UpladFilesTask().execute(uploadfile);
							 }
						});
						//new UpladFilesTask().execute(flists[i]);	//start asynchronous upload	 
					}catch (Exception e){
						e.printStackTrace();
					}
				}
				if(flists.length>0){
					updateServiceStatus(3); 
					while (runningtasks > 0){};
					updateServiceStatus(4); 
				}
				else{
					updateServiceStatus(5);
				}
				
				if(!buttonPlay){enableButton(R.id.playback);buttonPlay=true;}
				if(!buttonExit){enableButton(R.id.exit);buttonExit=true;}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	    
	}
 
	

	////////////////////////////////////////////////Asynchronous Upload task///////////////////////////////////////////////
	private class UpladFilesTask extends AsyncTask<File, Integer, String> {

		private String sendFile(File file)
		{			
			HttpURLConnection connection = null;
			DataOutputStream outputStream = null;
		
			String pathToOurFile = file.getAbsolutePath();
			String fileName = file.getName();

			String urlServer = "abc";
			String lineEnd = "\r\n";
			String twoHyphens = "--";
			String boundary =  "*****";

			int bytesRead, bytesAvailable, bufferSize;
			byte[] buffer;
			int maxBufferSize = 1*1024*1024;
		
			try
			{
				FileInputStream fileInputStream = new FileInputStream(new File(pathToOurFile) );

				URL url = new URL(urlServer);
				connection = (HttpURLConnection) url.openConnection();

				// Allow Inputs & Outputs
				connection.setDoInput(true);
				connection.setDoOutput(true);
				connection.setUseCaches(false);

				// Enable POST method
				connection.setRequestMethod("POST");

				connection.setRequestProperty("Connection", "Keep-Alive");
				connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);

				outputStream = new DataOutputStream( connection.getOutputStream() );
				outputStream.writeBytes(twoHyphens + boundary + lineEnd);
				outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + fileName +"\"" + lineEnd);
				outputStream.writeBytes(lineEnd);

				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// Read file
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0)
				{
					outputStream.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
				}

				outputStream.writeBytes(lineEnd);
				outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				int serverResponseCode = connection.getResponseCode();
				String serverResponseMessage = connection.getResponseMessage();

				fileInputStream.close();
				outputStream.flush();
				outputStream.close();
			
				file.delete(); //delete file
				return "Success";
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
			return "Error";
		} 

		@Override
		protected String doInBackground(File... file) {
			if (file[0].exists())
				return sendFile(file[0]);
			else 
				return "No file";
		}
		
		// This is called when doInBackground() is finished
		protected void onPostExecute(String result) {
			if (result.equals("Success"))
				synchronized(this){runningtasks--;}	
		}
	};

}
